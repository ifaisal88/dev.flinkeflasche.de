<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->foreignId('product_id')->constrained('products');
            $table->foreignId('parent_id')->constrained('product_reviews');
            $table->foreignId('user_id')->constrained('users');
            $table->string('reviews')->required();
            $table->float('ratings')->required();
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->boolean('status')->required()->default(true); // <-- This will add a status field either TRUE or FALSE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
