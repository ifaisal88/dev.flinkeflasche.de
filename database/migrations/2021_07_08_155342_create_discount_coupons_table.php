<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_coupons', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->foreignId('availed_by_user_id')->nullable()->constrained('users');
            $table->string('name')->required();
            $table->float('discount_percentage')->nullable()->default(null);
            $table->float('discount_amount')->nullable()->default(null);
            $table->string('order_number')->nullable()->default(null);
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->boolean('status')->default(true); // <-- This will add a status field either TRUE or FALSE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupons');
    }
}
