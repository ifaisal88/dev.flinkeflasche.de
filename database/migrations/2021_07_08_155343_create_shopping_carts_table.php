<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_carts', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('coupon_id')->constrained('discount_coupons');
            $table->foreignId('payment_method_id')->constrained('payment_methods');
            $table->string('order_number')->required();
            $table->integer('total_quantity')->required();
            $table->float('total_price')->required();
            $table->float('total_tax')->required();
            $table->float('total_discount')->required();
            $table->float('shipping_price')->required();
            $table->float('deposit')->required();
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->boolean('status')->required()->default(true); // <-- This will add a status field either TRUE or FALSE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_carts');
    }
}
