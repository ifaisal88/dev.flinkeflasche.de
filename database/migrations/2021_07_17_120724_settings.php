<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->string('name')->required();
            $table->string('address')->required();
            $table->string('url')->required();
            $table->string('email')->required();
            $table->string('phone')->required();
            $table->string('fax')->required();
            $table->string('return_policy')->required();
            $table->string('currency')->required();
            $table->string('logo')->required();
            $table->string('banner1')->nullable();
            $table->string('banner2')->nullable();
            $table->string('banner3')->nullable();
            $table->string('banner4')->nullable();
            $table->string('banner5')->nullable();
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
