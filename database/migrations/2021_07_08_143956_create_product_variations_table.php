<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->foreignId('product_id')->constrained('products');
            $table->string('variation_type')->required();
            $table->string('name')->required();
            $table->string('image')->nullable();
            $table->float('price')->required();
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->boolean('status')->required()->default(true); // <-- This will add a status field either TRUE or FALSE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}
