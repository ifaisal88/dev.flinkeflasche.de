<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id(); // <-- This is the primary key of the table
            $table->foreignId('category_id')->constrained('categories');
            $table->string('item_number')->required();
            $table->string('name')->required();
            $table->float('unit')->nullable()->default(null);
            $table->float('customer_price')->nullable()->default(null);
            $table->float('deposit')->nullable()->default(null);
            $table->boolean('is_returnable')->nullable()->default(true);
            $table->float('vat')->nullable()->default(null);
            $table->float('base_price')->required();
            $table->string('description')->nullable()->default(null);
            $table->string('ingredients')->nullable()->default(null);
            $table->timestamps(); // <-- This will add a created_at and updated_at fields
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->boolean('status')->required()->default(true); // <-- This will add a status field either TRUE or FALSE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
