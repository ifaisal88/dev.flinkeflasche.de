<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = Setting::all();
        if(count($settings) == 0) {
            DB::table('settings')->insert([
                'name' => 'Shop Name',
                'address' => 'Shop Address',
                'url' => 'www.flinkeflasche.de',
                'email' => 'info@flinkeflasche.de',
                'phone' => '+1234567890',
                'fax' => '+1234567890',
                'return_policy' => 'This is our return policy',
                'currency' => 'US$',
                'logo' => 'logo',
                'banner1' => 'banner',
                'banner2' => 'banner',
                'banner3' => 'banner',
                'banner4' => 'banner',
                'banner5' => 'banner',
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
