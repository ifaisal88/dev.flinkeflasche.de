<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VariationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required'],
            'variation_type' => ['required'],
            'name' => ['required', Rule::unique('product_variations')->ignore($this->name)],
            'price' => ['required'],
            'status' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute is een verplicht veld ',
        ];
    }
}
