<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => ['required'],
            'name' => ['required', Rule::unique('categories')->ignore($this->name)],
            'item_number' => ['required'],
            'base_price' => ['required'],
            'status' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute Ist ein Pflichtfeld',
        ];
    }
}
