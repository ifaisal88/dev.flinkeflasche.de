<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'url' => 'required',
            'email' => 'required|email',
            'phone' => 'required|string',
            'fax' => 'required|string',
            'currency' => 'required|string|max:3',
            'return_policy' => 'required|string|max:255',
            'logo' => 'nullable|string',
            'banner1' => 'nullable|string',
            'banner2' => 'nullable|string',
            'banner3' => 'nullable|string',
            'banner4' => 'nullable|string',
            'banner5' => 'nullable|string'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute Ist ein Pflichtfeld.',
            'email' => 'Die E-Mail-Adresse muss eine gültige E-Mail-Adresse sein.',
            'max' => 'Die Währung darf 3 Zeichen nicht überschreiten.'
        ];
    }
}
