<?php

namespace App\Http\Controllers;

use App\Models\ItemDiscount;
use Illuminate\Http\Request;

class ItemDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ItemDiscount  $itemDiscount
     * @return \Illuminate\Http\Response
     */
    public function show(ItemDiscount $itemDiscount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ItemDiscount  $itemDiscount
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemDiscount $itemDiscount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ItemDiscount  $itemDiscount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemDiscount $itemDiscount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ItemDiscount  $itemDiscount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemDiscount $itemDiscount)
    {
        //
    }
}
