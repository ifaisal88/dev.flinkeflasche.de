<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\ProductVariation;
use App\Models\ProductImage;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductEditRequest;
use App\Http\Requests\VariationRequest;
use App\Http\Requests\ProductImageRequest;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('products', ['products' => $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        Product::create($request->all());
        return redirect()->back()->with('message', 'Product met succes gemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $categories = Category::all();
        $data = Product::find($product->id);
        return view('edit-product', ['data' => $data, 'categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEditRequest $request, Product $product)
    {
        $request->merge(['id' => $product->id]);
        Product::updateOrCreate(['id' => $product->id], [
            'category_id' => $request->category_id,
            'item_number' => $request->item_number,
            'name' => $request->name,
            'unit' => $request->unit,
            'base_price' => 0,
            'customer_price' => $request->customer_price,
            'deposit' => $request->deposit,
            'is_returnable' => $request->is_returnable,
            'vat' => $request->vat,
            'description' => $request->description,
            'ingredients' => $request->ingredients,
            'status' => $request->status,
            'pet_size' => $request->pet_size,
            'is_new' => $request->is_new,
            'recycable' => $request->recycable,
            'additional_information' => $request->additional_information,
            'manufactoral_description' => $request->manufactoral_description
        ]);
        return redirect()->back()->with('message', 'Product is succesvol bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function productVariation(Product $product)
    {
        $variations = ProductVariation::where('product_id', $product->id)->get();
        return view('product_variations', ['variations' => $variations, 'product' => $product]);
    }

    public function storeVariation(VariationRequest $request)
    {
        if($request->hasFile('image_variation'))
        {
            $fileName = time() . '.' . $request->file('image_variation')->getClientOriginalExtension();
            $path = public_path('/storage/variations/' . $fileName);
            Image::make($request->file('image_variation'))->resize(400, 400)->save($path);
            $request->merge(['image' => $fileName]);
            ProductVariation::create($request->except('image_variation'));
            return redirect()->back()->with('message', 'Produktvariante erfolgreich erstellt ');
        } else
        {
            return redirect()->back()->with('message', 'Mit dem Bild stimmt etwas nicht');
        }
    }

    public function showVariation(ProductVariation $variation)
    {
        $products = Product::all();
        $data = ProductVariation::find($variation->id);
        return view('edit-variation', ['data' => $data, 'products' => $products]);
    }

    public function variationUpdate(VariationRequest $request, ProductVariation $variation)
    {
        $request->merge(['id' => $variation->id]);
        $variation = ProductVariation::updateOrCreate(['id' => $request->id], [
            'product_id' => $request->product_id,
            'variation_type' => $request->variation_type,
            'name' => $request->name,
            'price' => $request->price,
            'status' => $request->status,
        ]);

        if($request->hasFile('image_variation'))
        {
            $fileName = ProductVariation::where('id', $variation->id)->first('image');
            $path = 'storage/variations/' . $fileName->image;
            unlink($path);
            $image = $request->file('image_variation');
            $fileName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save(public_path('storage/variations/' . $fileName));

            $variation->image = $fileName;
            $variation->save();
            $variation = ProductVariation::updateOrCreate(['id' => $request->id], [
                'image' => $fileName
            ]);
            return redirect()->back()->with('message', 'Produktvariante erfolgreich erstellt');
        }
    }

    public function showImages($product)
    {
        $product_id = $product;
        $images = ProductImage::where('product_id', $product)->get();
        return view('product-images', ['images' => $images, 'product_id' => $product_id]);
    }

    public function storeImage(ProductImageRequest $request, $product)
    {
        if($request->hasFile('product_image'))
        {
            $extension = $request->file('product_image')->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $size = getimagesize($request->file('product_image'));
            $size = $size[0] . ' X ' . $size[1];
            $path = public_path('/storage/products/' . $fileName);
            Image::make($request->file('product_image'))->save($path);
            $request->merge(['product_id' => $product, 'name' => $fileName, 'size' => $size, 'extension' => $extension, 'status' => 1]);
            ProductImage::create($request->all());
            return redirect()->back()->with('message', 'Produkt image erfolgreich erstellt ');
        } else
        {
            return redirect()->back()->with('message', 'Mit dem Bild stimmt etwas nicht');
        }
    }

    public function deleteImage(ProductImage $image)
    {
        $image = ProductImage::where('id', $image->id)->first();
        $path = 'storage/products/' . $image->name;
        unlink($path);
        $image->forceDelete();
        return redirect()->back()->with('message', 'Produkt Bild gelöscht');
    }
}
