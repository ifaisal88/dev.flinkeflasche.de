<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $categories = Category::all();
        return view('categories', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if($request->hasFile('category_image'))
        {
            $fileName = time() . '.' . $request->file('category_image')->getClientOriginalExtension();
            $path = public_path('/storage/categories/' . $fileName);
            Image::make($request->file('category_image'))->save($path);
            $request->merge(['image' => $fileName]);
            Category::create($request->except('category_image'));
            return redirect()->back()->with('message', 'Produktvariante erfolgreich erstellt ');
        } else
        {
            return redirect()->back()->with('message', 'Mit dem Bild stimmt etwas nicht');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $data = Category::find($category->id);
        return view('edit-category', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $request->merge(['id' => $category->id]);
        $category = Category::updateOrCreate(['id' => $request->id], [
            'name' => $request->name,
            'status' => $request->status,
        ]);

        if($request->hasFile('category_image'))
        {
            $fileName = Category::where('id', $category->id)->first('image');
            $path = 'storage/categories/' . $fileName->image;
            $image = $request->file('category_image');
            unlink($path);
            $fileName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(public_path('storage/categories/' . $fileName));

            $category->image = $fileName;
            $category->save();
            $category = Category::updateOrCreate(['id' => $request->id], [
                'image' => $fileName
            ]);
            return redirect()->back()->with('message', 'Produktvariante erfolgreich erstellt');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
