<?php

namespace App\Http\Controllers;

use App\Models\DiscountCoupon;
use App\Http\Requests\CouponRequest;

class DiscountCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = DiscountCoupon::all();
        return view('coupons', ['coupons' => $coupons]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        DiscountCoupon::create($request->all());
        return redirect()->back()->with('message', 'Coupon succesvol aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DiscountCoupon  $discountCoupon
     * @return \Illuminate\Http\Response
     */
    public function show(DiscountCoupon $coupon)
    {
        $data = DiscountCoupon::find($coupon->id);
        return view('edit-coupon', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DiscountCoupon  $discountCoupon
     * @return \Illuminate\Http\Response
     */
    public function edit(DiscountCoupon $discountCoupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DiscountCoupon  $discountCoupon
     * @return \Illuminate\Http\Response
     */
    public function update(CouponRequest $request, DiscountCoupon $coupon)
    {
        $request->merge(['id' => $coupon->id]);
        DiscountCoupon::updateOrCreate(['id' => $coupon->id], [
            'name' => $request->name,
            'discount_percentage' => $request->discount_percentage,
            'discount_amount' => $request->discount_amount,
            'status' => $request->status,
        ]);
        return redirect()->back()->with('message', 'Gutschein erfolgreich aktualisiert ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DiscountCoupon  $discountCoupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(DiscountCoupon $discountCoupon)
    {
        //
    }
}
