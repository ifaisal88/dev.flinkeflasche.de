<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Setting::first();
        return view('settings', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, Setting $setting)
    {
        $request->merge(['id' => $setting->id]);
        Setting::updateOrCreate(['id' => $request->id], [
            'name' => $request->name,
            'address' => $request->address,
            'url' => $request->url,
            'email' => $request->email,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'return_policy' => $request->return_policy,
            'currency' => $request->currency,
            // 'logo' => $request->logo,
            // 'banner1' => $request->banner1,
            // 'banner2' => $request->banner2,
            // 'banner3' => $request->banner3,
            // 'banner4' => $request->banner4,
            // 'banner5' => $request->banner5
        ]);
        return redirect()->back()->with('message', 'Einstellungen wurden erfolgreich aktualisiert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
