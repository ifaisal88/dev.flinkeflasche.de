<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShippingDetail
 * 
 * @property int $id
 * @property int $user_id
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property string $name
 * @property string|null $company_name
 * @property string|null $street
 * @property string $address
 * @property string|null $landline_number
 * @property string $mobile_number
 * @property string|null $ordering_information
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property City $city
 * @property Country $country
 * @property State $state
 * @property User $user
 *
 * @package App\Models
 */
class ShippingDetail extends Model
{
	use SoftDeletes;
	protected $table = 'shipping_details';

	protected $casts = [
		'user_id' => 'int',
		'country_id' => 'int',
		'state_id' => 'int',
		'city_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'user_id',
		'country_id',
		'state_id',
		'city_id',
		'name',
		'company_name',
		'street',
		'address',
		'landline_number',
		'mobile_number',
		'ordering_information',
		'status'
	];

	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function state()
	{
		return $this->belongsTo(State::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
