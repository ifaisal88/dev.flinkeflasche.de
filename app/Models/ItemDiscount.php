<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ItemDiscount
 * 
 * @property int $id
 * @property int $product_id
 * @property int $availed_by_user_id
 * @property int|null $start_range
 * @property int|null $end_range
 * @property float|null $discount_percentage
 * @property float|null $discount_amount
 * @property string|null $order_number
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property User $user
 * @property Product $product
 *
 * @package App\Models
 */
class ItemDiscount extends Model
{
	use SoftDeletes;
	protected $table = 'item_discounts';

	protected $casts = [
		'product_id' => 'int',
		'availed_by_user_id' => 'int',
		'start_range' => 'int',
		'end_range' => 'int',
		'discount_percentage' => 'float',
		'discount_amount' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'availed_by_user_id',
		'start_range',
		'end_range',
		'discount_percentage',
		'discount_amount',
		'order_number',
		'status'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'availed_by_user_id');
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
