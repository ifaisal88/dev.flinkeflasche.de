<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 *
 * @property int $id
 * @property int $category_id
 * @property string $item_number
 * @property string $name
 * @property float $unit
 * @property float $customer_price
 * @property float $deposit
 * @property bool $is_returnable
 * @property float $vat
 * @property float $base_price
 * @property string $description
 * @property string $ingredients
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 *
 * @property Category $category
 * @property Collection|ItemDiscount[] $item_discounts
 * @property Collection|ProductImage[] $product_images
 * @property Collection|ProductReview[] $product_reviews
 * @property Collection|ProductVariation[] $product_variations
 *
 * @package App\Models
 */
class Product extends Model
{
	use SoftDeletes;
	protected $table = 'products';

	protected $casts = [
		'category_id' => 'int',
		'unit' => 'float',
		'customer_price' => 'float',
		'deposit' => 'float',
		'is_returnable' => 'bool',
		'vat' => 'float',
		'base_price' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'category_id',
		'item_number',
		'name',
		'unit',
		'customer_price',
		'deposit',
		'is_returnable',
		'vat',
		'base_price',
		'description',
		'ingredients',
		'status',
        'is_new',
        'recycable',
        'additional_information',
        'manufactoral_description',
        'pet_size'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function item_discounts()
	{
		return $this->hasMany(ItemDiscount::class);
	}

	public function product_images()
	{
		return $this->hasMany(ProductImage::class);
	}

	public function product_reviews()
	{
		return $this->hasMany(ProductReview::class);
	}

	public function product_variations()
	{
		return $this->hasMany(ProductVariation::class);
	}
}
