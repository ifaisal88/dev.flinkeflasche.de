<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductVariation
 * 
 * @property int $id
 * @property int $product_id
 * @property string $variation_type
 * @property string $name
 * @property string $image
 * @property float $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property Product $product
 *
 * @package App\Models
 */
class ProductVariation extends Model
{
	use SoftDeletes;
	protected $table = 'product_variations';

	protected $casts = [
		'product_id' => 'int',
		'price' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'variation_type',
		'name',
		'image',
		'price',
		'status'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
