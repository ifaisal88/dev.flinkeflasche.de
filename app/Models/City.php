<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 * 
 * @property int $id
 * @property int $state_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property State $state
 * @property Collection|BillingDetail[] $billing_details
 * @property Collection|ShippingDetail[] $shipping_details
 *
 * @package App\Models
 */
class City extends Model
{
	use SoftDeletes;
	protected $table = 'cities';

	protected $casts = [
		'state_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'state_id',
		'name',
		'status'
	];

	public function state()
	{
		return $this->belongsTo(State::class);
	}

	public function billing_details()
	{
		return $this->hasMany(BillingDetail::class);
	}

	public function shipping_details()
	{
		return $this->hasMany(ShippingDetail::class);
	}
}
