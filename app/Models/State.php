<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class State
 * 
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property Country $country
 * @property Collection|BillingDetail[] $billing_details
 * @property Collection|City[] $cities
 * @property Collection|ShippingDetail[] $shipping_details
 *
 * @package App\Models
 */
class State extends Model
{
	use SoftDeletes;
	protected $table = 'states';

	protected $casts = [
		'country_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'country_id',
		'name',
		'status'
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function billing_details()
	{
		return $this->hasMany(BillingDetail::class);
	}

	public function cities()
	{
		return $this->hasMany(City::class);
	}

	public function shipping_details()
	{
		return $this->hasMany(ShippingDetail::class);
	}
}
