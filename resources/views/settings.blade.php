@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">{{ __('settings.shop_settings') }}</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{ route('settings.form.update', $data->id) }}" method="post">
                            <div class="modal-body">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.name') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.name') }}" name="name" id="name" value="{{ old('name', $data->name) }}" />
                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.address') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.address') }}" name="address" id="address" value="{{ old('address', $data->address) }}" />
                                        @error('address') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.url') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.url') }}" name="url" id="url" value="{{ old('url', $data->url) }}" />
                                        @error('url') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.email') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.email') }}" name="email" id="email" value="{{ old('email', $data->email) }}" />
                                        @error('email') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.phone') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.phone') }}" name="phone" id="phone" value="{{ old('phone', $data->phone) }}" />
                                        @error('phone') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.fax') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.fax') }}" name="fax" id="fax" value="{{ old('fax', $data->fax) }}" />
                                        @error('fax') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.return_policy') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.return_policy') }}" name="return_policy" id="return_policy" value="{{ old('return_policy', $data->return_policy) }}" />
                                        @error('return_policy') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>{{ __('settings.currency_symbol') }} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="{{ __('settings.currency_symbol') }}" name="currency" id="currency" value="{{ old('currency', $data->currency) }}" />
                                        @error('currency') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <div class="dropzone dropzone-default dropzone-success dz-clickable" id="kt_dropzone_3">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">{{ __('settings.drop_logo') }}</h3>
                                            <span class="dropzone-msg-desc">{{ __('settings.only_images') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 form-group">
                                    <div class="dropzone dropzone-default dropzone-success dz-clickable" id="kt_dropzone_3">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">{{ __('settings.drop_banner') }}</h3>
                                            <span class="dropzone-msg-desc">{{ __('settings.only_images') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{ __('settings.close') }}</button>
                                <input type="submit" value="{{ __('settings.save_changes') }}" class="btn btn-primary font-weight-bold">
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
