@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">{{ __('product-variations.product_variations') }}</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">{{ __('product-variations.page_heading') }}
                            <span class="d-block text-muted pt-2 font-size-sm">{{ __('product-variations.page_description') }}</span></h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#variationModal">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>{{ __('product-variations.new_record') }}</a>
                            <!--end::Button-->
                            <!--begin::Modal to add category-->
                            <div class="modal fade" id="variationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('product-variations.product_variations') }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('products.variation.store', $product) }}" method="post" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                @csrf
                                                <input type="hidden" id="product_id" name="product_id" value="{{ $product->id}}">
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('product-variations.variation_type') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('product-variations.variation_type') }}" name="variation_type" id="variation_type" />
                                                        @error('variation_type') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('product-variations.name') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('product-variations.name') }}" name="name" id="name" />
                                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('product-variations.price') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('product-variations.price') }}" name="price" id="price" />
                                                        @error('price') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('product-variations.status') }} <span class="text-danger">*</span></label>
                                                        <select class="form-control" id="kt_select2_2" name="status">
                                                            <option value="">{{ __('product-variations.select_status') }}</option>
                                                            <option value="1">{{ __('product-variations.active') }}</option>
                                                            <option value="0">{{ __('product-variations.in_active') }}</option>
                                                        </select>
                                                        @error('status') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <input type="file" class="varitionImage dropzone dropzone-default dropzone-success dz-clickable" name="image_variation" id="varitionImages" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{ __('product-variations.close') }}</button>
                                                <input type="submit" value="{{ __('product-variations.save_changes') }}" class="btn btn-primary font-weight-bold">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end::Modal to add category-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="{{ __('product-variations.search') }}..." id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0 d-none d-md-block">{{ __('product-variations.status') }}:</label>
                                                <select class="form-control" id="kt_datatable_search_status">
                                                    <option value="">{{ __('product-variations.all') }}</option>
                                                    <option value="1">{{ __('product-variations.in_active') }}</option>
                                                    <option value="0">{{ __('product-variations.active') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">{{ __('product-variations.search') }}</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <!--begin: Datatable-->
                        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th title="Field #1">{{ __('product-variations.sr') }}</th>
                                    <th title="Field #2">{{ __('product-variations.image') }}</th>
                                    <th title="Field #3">{{ __('product-variations.variation_type') }}</th>
                                    <th title="Field #4">{{ __('product-variations.name') }}</th>
                                    <th title="Field #5">{{ __('product-variations.price') }}</th>
                                    <th title="Field #6">{{ __('product-variations.status') }}</th>
                                    <th title="Field #7">{{ __('product-variations.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $serial = 1;
                                @endphp
                                @foreach ($variations as $variation)
                                <tr>
                                    <td>{{ $serial }}</td>
                                    <td><img src="{{ asset('storage/variations/' . $variation->image) }}" widtn="25" height="25" /></td>
                                    <td>{{ $variation->variation_type }}</td>
                                    <td>{{ $variation->name }}</td>
                                    <td>{{ $variation->price }}</td>
                                    <td class="text-right">4</td>
                                    <td>
                                        <span style="overflow: visible; position: relative; width: 125px;">
                                            <a href="{{ route('products.variation.edit', $variation) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit Category">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                            <a href="delete" class="btn btn-sm btn-clean btn-icon" title="Delete" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </span>
                                        <form id="delete-form" action="{{ route('products.variation.delete', $variation->product_id) }}" method="POST" style="display: none;">
                                            @method('DELETE');
                                            {{ csrf_field() }}
                                        </form>
                                    </td>
                                </tr>
                                @php
                                    $serial++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
@section('extra-js')
<script>
    $("#varitionImages").dropzone();
</script>
@endsection


