@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">{{ __('products.product') }}</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">{{ __('products.product_management') }}
                            <span class="d-block text-muted pt-2 font-size-sm">{{ __('products.page_description') }}</span></h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#productModal">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>{{ __('products.new_record') }}</a>
                            <!--end::Button-->
                            <!--begin::Modal to add category-->
                            <div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('products.product') }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form action="{{ route('products.form.store') }}" method="post">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('products.category') }} <span class="text-danger">*</span></label>
                                                        <select class="form-control" id="kt_select2_1" name="category_id">
                                                            <option value="">{{ __('products.select_category') }}</option>
                                                            @foreach ($categories as $category)
                                                                <option value="{{ $category->id }}">{{  $category->name }}</option>
                                                            @endforeach)
                                                        </select>
                                                        @error('category_id') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('products.item_number') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('products.item_number') }}" name="item_number" id="item_number" />
                                                        @error('item_number') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('products.name') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('products.name') }}" name="name" id="name" />
                                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>{{ __('products.base_price') }} <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control" placeholder="{{ __('products.base_price') }}" name="base_price" id="base_price" />
                                                        @error('base_price') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 form-group">
                                                        <label>Status <span class="text-danger">*</span></label>
                                                        <select class="form-control" id="kt_select2_2" name="status">
                                                            <option value="">{{ __('products.select_status') }}</option>
                                                            <option value="1">{{ __('products.active') }}</option>
                                                            <option value="0">{{ __('products.in_active') }}</option>
                                                        </select>
                                                        @error('status') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{ __('products.close') }}</button>
                                                <input type="submit" value="{{ __('products.save_changes') }}" class="btn btn-primary font-weight-bold">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end::Modal to add category-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="{{ __('products.search') }}..." id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0 d-none d-md-block">{{ __('products.status') }}:</label>
                                                <select class="form-control" id="kt_datatable_search_status">
                                                    <option value="">{{ __('products.all') }}</option>
                                                    <option value="1">{{ __('products.in_active') }}</option>
                                                    <option value="0">{{ __('products.active') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">{{ __('products.search') }}</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <!--begin: Datatable-->
                        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th title="Field #1">{{ __('products.sr') }}</th>
                                    <th title="Field #2">{{ __('products.name') }}</th>
                                    <th title="Field #5">{{ __('products.item_number') }}</th>
                                    {{-- <th title="Field #6">{{ __('products.base_price') }}</th> --}}
                                    <th title="Field #10">{{ __('products.status') }}</th>
                                    <th title="Field #11">{{ __('products.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $serial = 1;
                                @endphp
                                @foreach ($products as $product)
                                <tr>
                                    <td>{{ $serial }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->item_number }}</td>
                                    {{-- <td>{{ $product->base_price }}</td> --}}
                                    <td class="text-right">4</td>
                                    <td>
                                        <span style="overflow: visible; position: relative; width: 125px;">
                                            <a href="{{ route('products.read.images', $product) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Product Images">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" x="2" y="4" width="20" height="16" rx="2"></rect>
                                                        <polygon fill="#000000" opacity="0.3" points="4 20 10.5 11 17 20"></polygon>
                                                        <polygon fill="#000000" points="11 20 15.5 14 20 20"></polygon>
                                                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="8.5" r="1.5"></circle>
                                                    </g>
                                                </svg>
                                            </a>
                                            <a href="{{ route('products.show.productVariation', $product) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Product Variations">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect opacity="0.200000003" x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4.5,7 L9.5,7 C10.3284271,7 11,7.67157288 11,8.5 C11,9.32842712 10.3284271,10 9.5,10 L4.5,10 C3.67157288,10 3,9.32842712 3,8.5 C3,7.67157288 3.67157288,7 4.5,7 Z M13.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L13.5,18 C12.6715729,18 12,17.3284271 12,16.5 C12,15.6715729 12.6715729,15 13.5,15 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M17,11 C15.3431458,11 14,9.65685425 14,8 C14,6.34314575 15.3431458,5 17,5 C18.6568542,5 20,6.34314575 20,8 C20,9.65685425 18.6568542,11 17,11 Z M6,19 C4.34314575,19 3,17.6568542 3,16 C3,14.3431458 4.34314575,13 6,13 C7.65685425,13 9,14.3431458 9,16 C9,17.6568542 7.65685425,19 6,19 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                            <a href="{{ route('products.form.show', $product) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit Product">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                            <a href="{{ route('products.form.delete', $product) }}" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                @php
                                    $serial++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
