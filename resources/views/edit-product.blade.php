@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Edit Product</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                    <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{ route('products.form.update', $data->id) }}" method="post">
                            <div class="modal-body">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Category <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_1" name="category_id">
                                            <option value="">Select Category</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" {{ $category->id == old("category_id", $data->category_id) ? "selected" : "" }}>{{ $category->name }}</option>
                                            @endforeach)
                                        </select>
                                        @error('category_id') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Item Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Item Number" name="item_number" id="item_number" value="{{ old('item_number', $data->item_number) }}" />
                                        @error('item_number') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name', $data->name) }}" />
                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Unit <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Unit" name="unit" id="unit" value="{{ old('unit', $data->unit) }}" />
                                        @error('unit') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    {{-- <div class="col-6 form-group">
                                        <label>Base Price <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Base Price" name="base_price" id="base_price" value="{{ old('base_price', $data->base_price) }}" />
                                        @error('base_price') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div> --}}
                                    <div class="col-6 form-group">
                                        <label>Customer Price <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Customer Price" name="customer_price" id="customer_price" value="{{ old('customer_price', $data->customer_price) }}" />
                                        @error('customer_price') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Pet Size <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Pet Size" name="pet_size" id="pet_size" value="{{ old('pet_size', $data->pet_size) }}" />
                                        @error('pet_size') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>VAT <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Vat" name="vat" id="vat" value="{{ old('vat', $data->vat) }}" />
                                        @error('vat') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Deposit <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Deposit" name="deposit" id="deposit" value="{{ old('deposit', $data->deposit) }}" />
                                        @error('deposit') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Description <span class="text-danger">*</span></label>
                                        {{-- <input type="text" class="form-control" placeholder="Description" name="description" id="description" value="{{ old('description', $data->description) }}" /> --}}
                                        <textarea class="form-control" name="description" id="description">{{ old('description', $data->description) }}</textarea>
                                        @error('description') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Ingredients <span class="text-danger">*</span></label>
                                        {{-- <input type="text" class="form-control" placeholder="Ingredients" name="ingredients" id="ingredients" value="{{ old('ingredients', $data->ingredients) }}" /> --}}
                                        <textarea class="form-control" name="ingredients" id="ingredients">{{ old('ingredients', $data->ingredients) }}</textarea>
                                        @error('ingredients') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Manufactorial Description <span class="text-danger">*</span></label>
                                        {{-- <input type="text" class="form-control" placeholder="Ingredients" name="ingredients" id="ingredients" value="{{ old('ingredients', $data->ingredients) }}" /> --}}
                                        <textarea class="form-control" name="manufactoral_description" id="manufactoral_description">{{ old('manufactoral_description', $data->manufactoral_description) }}</textarea>
                                        @error('manufactoral_description') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Additional Information <span class="text-danger">*</span></label>
                                        {{-- <input type="text" class="form-control" placeholder="Ingredients" name="ingredients" id="ingredients" value="{{ old('ingredients', $data->ingredients) }}" /> --}}
                                        <textarea class="form-control" name="additional_information" id="additional_information">{{ old('additional_information', $data->additional_information) }}</textarea>
                                        @error('additional_information') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Is Returnable? <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_2" name="is_returnable">
                                            <option value="">Select Category</option>
                                            <option value="0" {{ 0 == old("is_returnable", $data->is_returnable) ? "selected" : "" }}>No</option>
                                            <option value="1" {{ 1 == old("is_returnable", $data->is_returnable) ? "selected" : "" }}>Yes</option>
                                        </select>
                                        @error('is_returnable') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_3" name="status">
                                            <option value="">Select Status</option>
                                            <option value="0" {{ 0 == old("status", $data->status) ? "selected" : "" }}>In-Active</option>
                                            <option value="1" {{ 1 == old("status", $data->status) ? "selected" : "" }}>Active</option>
                                        </select>
                                        @error('status') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Is New? <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_4" name="is_new">
                                            <option value="">Select Recycable</option>
                                            <option value="0" {{ 0 == old("is_new", $data->is_new) ? "selected" : "" }}>No</option>
                                            <option value="1" {{ 1 == old("is_new", $data->is_new) ? "selected" : "" }}>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Is Recycable? <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_1" name="recycable">
                                            <option value="">Select Recycable</option>
                                            <option value="0" {{ 0 == old("recycable", $data->recycable) ? "selected" : "" }}>No</option>
                                            <option value="1" {{ 1 == old("recycable", $data->recycable) ? "selected" : "" }}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {{-- <a href="{{ url()->previous() }}" class="btn btn-light-primary font-weight-bold">Back to Products</a> --}}
                                <input type="submit" value="Save changes" class="btn btn-primary font-weight-bold">
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
