<!DOCTYPE html>
<html lang="en">
<head>
<x-header-component></x-header-component>
@yield('extra-css')
        <div class="d-flex flex-column flex-root">
            <!--begin::Page-->
            <div class="d-flex flex-row flex-column-fluid page">
                <!--begin::Aside-->
                <x-aside-component></x-aside-component>
                <!--end::Aside-->
                <!--begin::Wrapper-->
                <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                    <!--begin::Header-->
                    <x-header-menu-component></x-header-menu-component>
                    <!--end::Header-->

                    @yield('content')
<x-footer-component></x-footer-component>
@yield('extra-js')
</body>
<!--end::Body-->
</html>
