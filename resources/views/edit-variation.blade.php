@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Edit Variation</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                    <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{ route('products.variation.update', $data->id) }}" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Product <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_1" name="product_id">
                                            <option value="">Select Product</option>
                                            @foreach ($products as $product)
                                                <option value="{{ $product->id }}" {{ $product->id == old("product_id", $data->product_id) ? "selected" : "" }}>{{ $product->name }}</option>
                                            @endforeach)
                                        </select>
                                        @error('product_id') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Variation Type <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Variation Type" name="variation_type" id="variation_type" value="{{ old('variation_type', $data->variation_type) }}" />
                                        @error('variation_type') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Product Name" name="name" id="name" value="{{ old('name', $data->name) }}" />
                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Price <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Price" name="price" id="price" value="{{ old('price', $data->price) }}" />
                                        @error('price') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_2" name="status">
                                            <option value="">Select Status</option>
                                            <option value="1" {{ 1 == old("status", $data->status) ? "selected" : "" }}>Active</option>
                                            <option value="0" {{ 0 == old("status", $data->status) ? "selected" : "" }}>In-Active</option>
                                        </select>
                                        @error('status') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <input type="file" class="varitionImage dropzone dropzone-default dropzone-success dz-clickable" name="image_variation" id="varitionImages" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ URL::previous() }}" class="btn btn-light-primary font-weight-bold">Back to Variations</a>
                                <input type="submit" value="Save changes" class="btn btn-primary font-weight-bold">
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
@section('extra-js')
<script>
    $("#varitionImages").dropzone();
</script>
@endsection
