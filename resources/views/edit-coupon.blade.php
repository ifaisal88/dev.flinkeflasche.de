@extends('layouts.template')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Breadcrumbs-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Edit Coupon</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Breadcrumbs-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{ route('coupons.form.update', $data->id) }}" method="post">
                            <div class="modal-body">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name', $data->name) }}" />
                                        @error('name') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Discount Percentage <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Discount Percentage" name="discount_percentage" id="discount_percentage" value="{{ old('discount_percentage', $data->discount_percentage) }}" />
                                        @error('discount_percentage') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label>Discount Amount <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Discount Amount" name="discount_amount" id="discount_amount" value="{{ old('discount_amount', $data->discount_amount) }}" />
                                        @error('discount_amount') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-6 form-group">
                                        <label>Status <span class="text-danger">*</span></label>
                                        <select class="form-control" id="kt_select2_2" name="status">
                                            <option value="">Select Status</option>
                                            <option value="1" {{ 1 == old("status", $data->status) ? "selected" : "" }}>Active</option>
                                            <option value="0" {{ 0 == old("status", $data->status) ? "selected" : "" }}>In-Active</option>
                                        </select>
                                        @error('status') <span class="form-text text-danger">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                    <input type="submit" value="Save changes" class="btn btn-primary font-weight-bold">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection
