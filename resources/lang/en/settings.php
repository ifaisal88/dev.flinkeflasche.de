<?php
    return [
        'shop_settings' => 'Shop Settings',
        'name' => 'Name',
        'address' => 'Address',
        'email' => 'Email',
        'url' => 'URL',
        'phone' => 'Phone',
        'fax' => 'Fax',
        'return_policy' => 'Return Policy',
        'currency_symbol' => 'Currency Symbol',
        'save_changes' => 'Save Changes',
        'close' => 'Close',
        'drop_logo' => 'Drop Logo here or click to upload',
        'drop_banner' => 'Drop Banner here or click to upload',
        'only_images' => 'Only images are allowed for upload'
    ]
?>
