<?php
    return [
        'product' => 'Product',
        'product_management' => 'Product Management',
        'page_description' => 'Page to manage all Products',
        'search' => 'Search',
        'new_record' => 'New Record',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Action',
        'empty_table' => 'No Records found',
        'active' => 'Active',
        'in_active' => 'In-Active',
        'select_status' => 'Select Status',
        'select_category' => 'Select Category',
        'close' => 'Close',
        'save_changes' => 'Save Changes',
        'all' => 'All',
        'item_number' => 'Item Number',
        'base_price' => 'Base Price'
    ]
?>
