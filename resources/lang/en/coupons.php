<?php
    return
    [
        'discount_coupon' => 'Discount Coupons',
        'coupon_management' => 'Coupons Management ',
        'page_description' => 'Page to manage all coupons',
        'search' => 'Search',
        'new_record' => 'New Record',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Action',
        'empty_table' => 'No records found',
        'active' => 'Active',
        'in_active' => 'In-Active',
        'select_status' => 'Select Status',
        'select_category' => 'Select Category',
        'close' => 'Close',
        'all' => 'All',
        'save_changes' => 'Save Changes',
        'dis_percent' => 'Discount in %',
        'dis_amt' => 'Discount in Amount',
        'coupon' => 'Coupon',
        'coupon_name' => 'Coupon Name'
    ]
?>
