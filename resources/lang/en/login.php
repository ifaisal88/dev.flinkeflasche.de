<?php
    return [
        'email' => 'email',
        'password' => 'password',
        'remember' => 'Remember me',
        'forgot' => 'Forgot your password?',
        'login' => 'LOG IN'
    ]
?>
