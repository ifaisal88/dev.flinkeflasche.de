<?php
return [
    'title' => 'Product Images',
    'heading' => 'Image Management',
    'description' => 'Page to manage all images',
    'new_record' => 'New Record',
    'image' => 'Image',
    'browse' => 'Browse',
    'file' => 'No File Selected',
    'close' => 'Close',
    'save_changes' => 'Save Changes',
    'search' => 'Search',
    'status' => 'Status',
    'empty_table' => 'No Records found',
    'active' => 'Active',
    'in_active' => 'In-Active',
    'all' => 'All',
    'sr' => "Sr",
    'name' => 'Name',
    'size' => 'Size',
    'extension' => 'Extension',
    'action' => 'Action',
    'delete' => 'Delete'
]
?>
