<?php
    return [
        'product_variations' => 'Product Variations',
        'page_heading' => 'Product Variation Management',
        'page_description' => 'Page to manage Product Variations','search' => 'Search',
        'new_record' => 'New Record',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Action',
        'empty_table' => 'No Records found',
        'active' => 'Active',
        'in_active' => 'In-Active',
        'select_status' => 'Select Status',
        'image' => 'Image',
        'price' => 'Price',
        'variation_type' => 'Variation Type',
        'product_name' => 'Product Name',
        'all' => 'All',
        'close' => 'Close',
        'save_changes' => 'Save Changes'
    ]
?>
