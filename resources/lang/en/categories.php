<?php
    return [
        'category' => 'Category',
        'category_management' => 'Category Management',
        'page_description' => 'Page to manage all categories',
        'search' => 'Search',
        'new_record' => 'New Record',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Action',
        'empty_table' => 'No Records found',
        'active' => 'Active',
        'in_active' => 'In-Active',
        'category_name' => 'Category Name',
        'select_status' => 'Select Status',
        'close' => 'Close',
        'all' => 'All',
        'save_changes' => 'Save Changes',
        'image' => 'Image',
        'parent' => 'Parent Category',
        'select_parent' => 'Select Parent'
    ]
?>
