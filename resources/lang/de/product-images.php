<?php
return [
    'title' => 'Produktbilder',
    'heading' => 'Bildverwaltung',
    'description' => 'Seite zum Verwalten aller Bilder',
    'new_record' => 'Neuer Eintrag ',
    'image' => 'Bild',
    'browse' => 'Durchsuche',
    'file' => 'Keine Datei ausgewählt',
    'close' => 'Schließen',
    'save_changes' => 'Änderungen speichern',
    'search' => 'Suche',
    'status' => 'Status',
    'empty_table' => 'Keine Aufzeichnungen gefunden ',
    'active' => 'Aktiv',
    'in_active' => 'Inaktiv',
    'all' => 'Alle',
    'sr' => "Sr",
    'name' => 'Name',
    'size' => 'Größe',
    'extension' => 'Verlängerung',
    'action' => 'Aktion',
    'delete' => 'Löschen'
]
?>
