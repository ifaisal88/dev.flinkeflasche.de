<?php
    return [
        'shop_settings' => 'Shop-Einstellungen',
        'name' => 'Name',
        'address' => 'Adresse',
        'email' => 'E-mail',
        'url' => 'URL',
        'phone' => 'Telefon',
        'fax' => 'Faxgerät',
        'return_policy' => 'Rücknahmegarantie',
        'currency_symbol' => 'Währungszeichen',
        'save_changes' => 'Änderungen speichern',
        'close' => 'Schließen',
        'drop_logo' => 'Legen Sie das Logo hier ab oder klicken Sie zum Hochladen',
        'drop_banner' => 'Lassen Sie das Banner hier fallen oder klicken Sie zum Hochladen',
        'only_images' => 'Es dürfen nur Bilder hochgeladen werden'
    ]
?>
