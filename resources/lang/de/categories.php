<?php
    return [
        'category' => 'Kategorie',
        'category_management' => 'Kategorienverwaltung',
        'page_description' => 'Seite zum Verwalten aller Kategorien',
        'search' => 'Suche',
        'new_record' => 'Neuer Eintrag ',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Aktion',
        'empty_table' => 'Keine Aufzeichnungen gefunden ',
        'active' => 'Aktiv',
        'in_active' => 'Inaktiv',
        'category_name' => 'Categorie naam',
        'select_status' => 'Wählen Sie Status',
        'close' => 'nahe bei',
        'all' => 'alle',
        'save_changes' => 'Änderungen speichern',
        'image' => 'Bild',
        'parent' => 'Parent Category',
        'select_parent' => 'Select Parent'
    ]
?>
