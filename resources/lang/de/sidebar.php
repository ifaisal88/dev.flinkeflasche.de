<?php
    return [
        'dashboard' => 'Armaturenbrett',
        'product_management' => 'Produkt Management ',
        'products' => 'Produkte',
        'categories' => 'Kategorien',
        'products' => 'Produkte',
        'coupons' => 'Gutscheine',
        'settings' => 'Einstellungen'
    ]
?>
