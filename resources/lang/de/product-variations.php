<?php
    return [
        'product_variations' => 'Produktvariationen',
        'page_heading' => 'Produktvarianten-Management',
        'page_description' => 'Seite zum Verwalten von Produktvariationen ',
        'search' => 'Suchen',
        'new_record' => 'Neuer Eintrag ',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Aktion',
        'empty_table' => 'Keine Aufzeichnungen gefunden ',
        'active' => 'Aktiv',
        'in_active' => 'Inaktiv',
        'select_status' => 'Wählen Sie Status',
        'image' => 'Bild',
        'price' => 'Preis',
        'variation_type' => 'Variationstyp',
        'product_name' => 'Produktname',
        'close' => 'nahe bei',
        'all' => 'alle',
        'save_changes' => 'Änderungen speichern'
    ]
?>
