<?php
    return [
        'product' => 'Produkt',
        'product_management' => 'Produkt Management ',
        'page_description' => 'Seite zum Verwalten aller Kategorien ',
        'search' => 'Suchen',
        'new_record' => 'Neue Datei',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Aktion',
        'empty_table' => 'Keine Aufzeichnungen gefunden ',
        'active' => 'Aktiv',
        'in_active' => 'Inaktiv',
        'select_status' => 'Wählen Sie Status',
        'select_category' => 'Kategorie wählen',
        'close' => 'nahe bei',
        'all' => 'alle',
        'save_changes' => 'Änderungen speichern',
        'item_number' => 'Artikel nummer',
        'base_price' => 'Grundpreis'
    ]
?>
