<?php
    return
    [
        'discount_coupon' => 'Rabattgutschein',
        'coupon_management' => 'Gutscheinverwaltung ',
        'page_description' => 'Seite zum Verwalten aller Kategorien ',
        'search' => 'Suchen',
        'new_record' => 'Neue Datei ',
        'sr' => 'Sr',
        'name' => 'Name',
        'status' => 'Status',
        'action' => 'Aktion',
        'empty_table' => 'Keine Aufzeichnungen gefunden ',
        'active' => 'Aktiv',
        'in_active' => 'Inaktiv',
        'select_status' => 'Wählen Sie Status',
        'select_category' => 'Neue Datei',
        'close' => 'nahe bei',
        'all' => 'alle',
        'save_changes' => 'Änderungen speichern',
        'dis_percent' => 'Rabatt in  % ',
        'dis_amt' => 'Rabatt in Höhe ',
        'coupon' => 'Coupon',
        'coupon_name' => 'Namensgutschein'
    ]
?>
