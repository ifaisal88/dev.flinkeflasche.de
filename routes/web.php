<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DiscountCouponController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use App\Models\DiscountCoupon;
use App\Models\ProductVariation;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group( function(){
    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('read.index');
        Route::post('/categories', [CategoryController::class, 'store'])->name('form.store');
        Route::get('/{category}', [CategoryController::class, 'show'])->name('form.show');
        Route::patch('/{category}', [CategoryController::class, 'update'])->name('form.update');
        Route::delete('/categories/{category}', [CategoryController::class], 'destroy')->name('form.delete');
    });

    Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
        Route::get('/', [ProductController::class, 'index'])->name('read.index');
        Route::post('/products', [ProductController::class, 'store'])->name('form.store');
        Route::get('/{product}', [ProductController::class, 'show'])->name('form.show');
        Route::put('/{product}', [ProductController::class, 'update'])->name('form.update');
        Route::delete('/{product}', [ProductController::class, 'destroy'])->name('form.delete');

        Route::get('/{product}/variations', [ProductController::class, 'productVariation'])->name('show.productVariation');
        Route::post('/{variation}', [ProductController::class, 'storeVariation'])->name('variation.store');
        Route::get('variation/{variation}', [ProductController::class, 'showVariation'])->name('variation.edit');
        Route::patch('variation/{variation}', [ProductController::class, 'variationUpdate'])->name('variation.update');
        Route::delete('/product/{product}', [ProductController::class], 'destroy')->name('variation.delete');

        Route::get('/{product}/images', [ProductController::class, 'showImages'])->name('read.images');
        Route::post('/images/{product}', [ProductController::class, 'storeImage'])->name('store.image');
        Route::get('/images/{image}', [ProductController::class, 'deleteImage'])->name('images.destroy');
    });

    Route::group(['prefix' => 'coupons', 'as' => 'coupons.'], function () {
        Route::get('/', [DiscountCouponController::class, 'index'])->name('read.index');
        Route::post('/coupon', [DiscountCouponController::class, 'store'])->name('form.store');
        Route::get('/{coupon}', [DiscountCouponController::class, 'show'])->name('form.show');
        Route::put('/{coupon}', [DiscountCouponController::class, 'update'])->name('form.update');
        Route::delete('/coupon/{coupon}', [DiscountCouponController::class], 'destroy')->name('form.delete');
    });

    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/', [SettingController::class, 'index'])->name('read.index');
        Route::put('/{setting}', [SettingController::class, 'update'])->name('form.update');
    });
});
